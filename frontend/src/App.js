import React from 'react'
import { Grid } from '@material-ui/core'
import {Sidebar, CreateTest, Header} from './components'
function App() {
    return (
            <Grid container direction='column'>
                <Grid item>
                    <Header/>
                </Grid>

                <Grid item container>
                  <Grid item xs={1}>
                    <Sidebar/>
                  </Grid>

                  <Grid item xs={11}>
                    <CreateTest/>
                  </Grid>

                </Grid>
            </Grid>
    );
}

export default App;
