import React, { useState } from 'react'
import { TextField, Paper, Grid, Typography, Button, Card } from '@material-ui/core'
import EditorMail from '../Editor/Editor'
import parse from 'html-react-parser'
import axios from 'axios'
import {useForm, FormProvider} from 'react-hook-form'
import FormInput from '../CustomTextField'
import useStyles from './styles'

const Mail = ({handleClose1, disable}) => {
    const methods = useForm();
    const classes = useStyles()
    const [text, setText] = useState('')
    const onEditorStateChange = text =>{
        console.log(text)
       
    }
    const convert =( text ) =>{
        text = text.replace('$NAME$','Danh')
        text = text.replace('$SUBJECT$','Interview')
        text = text.replace('$QUESTIONS$','<p>Hoá trị của Hidro ? <p>')
        text = text.replace('$DURATION$','60 phút')
        text = text.replace('$URL$','https://eyeq.tech/eyeq-tech-tuyen-dung-2021/')
        text = text.replace('$DEADLINE$','1 tuần')
        return text
    }
    const submitTemplate = ( Template ) => {

    }
    const onSubmit =  data => {
        // console.log({...data,text})
        // return
            axios.post('https://secret-everglades-88476.herokuapp.com/template/add',{...data,text})
                .then(res => {
                    console.log(res.data)
                    handleClose1()
                })
                .catch(err => console.log(err))
            
       };
    return (
        <Paper className={classes.paper} style={{borderTop: '5px solid #3f51b5'}}>
            <Typography className={classes.typography}  variant='h6' align='center' >Add Email Template</Typography>
            <FormProvider { ...methods}>
            <form onSubmit={methods.handleSubmit(onSubmit)}>
                <Grid container spacing={1}>
                    <Grid item xs={12} sm={8}>
                        <FormInput name='name' label='Add a title' defaultValue='hahaha'/>
                        <br />
                        <EditorMail text = {text} setText={setText}/> 
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <Paper variant="outlined" fullWidth style={{ height: 200}} >
                            <Typography className={classes.typography} style={{borderBottom: '1px solid #e0e0e0'}} variant='body2' align='center'>Use these components to personalize your message</Typography>
                            <Typography  variant='subtitle2' >- $NAME$: student's name.</Typography>
                            <Typography  variant='subtitle2' >- $SUBJECT$: subject.</Typography>
                            <Typography  variant='subtitle2' >- $QUESTIONS$: questions.</Typography>
                            <Typography  variant='subtitle2' >- $DURATION$: test duration.</Typography>
                            <Typography  variant='subtitle2' >- $URL$: test url.</Typography>
                            <Typography  variant='subtitle2' >- $DEADLINE$: test's deadline.</Typography>
                        </Paper>
                    </Grid>

                    <Grid item xs={12}>
                    <Typography className={classes.typography} variant='h6' align='center'>You email will be looked like this...</Typography>
                        <Card variant="outlined" style={{ minHeight: 200}}>
                        <Typography  variant='subtitle2' >{parse(convert(text))}</Typography>
                        </Card>
                        <div className={classes.buttons}>
                            <Button className={classes.button} variant="contained" onClick={handleClose1}>Cancel</Button>
                            <Button className={classes.button} variant="contained" color="primary" type='submit' disabled={disable}>Save</Button>
                        </div>   
                    </Grid>
                </Grid>
                </form>
            </FormProvider>
        

    </Paper>
            
    )
}

export default Mail
