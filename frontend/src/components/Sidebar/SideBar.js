import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import { Grid } from '@material-ui/core'
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';
import ForumOutlinedIcon from '@material-ui/icons/ForumOutlined';
import AssignmentReturnedOutlinedIcon from '@material-ui/icons/AssignmentReturnedOutlined';
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';
import useStyles from './Styles'




export default function MiniDrawer() {
  const classes = useStyles();
  const SideBarData = [
    {
        title: "Home",
        icon: <HomeOutlinedIcon className={classes.iconSideBar}/>,
        link: '/'
    },
    {
        title: "User",
        icon: <PeopleAltOutlinedIcon className={classes.iconSideBar}/>,
        link: '/user'
    },
    {
        title: "Test",
        icon: <AssignmentOutlinedIcon className={classes.iconSideBar}/>,
        link: '/'
    },
    {
        title: "Chat",
        icon: <ForumOutlinedIcon className={classes.iconSideBar} />,
        link: '/'
    },
    {
        title: "Data",
        icon: <FileCopyOutlinedIcon className={classes.iconSideBar} />,
        link: '/'
    },
    {
        title: "Export",
        icon: <AssignmentReturnedOutlinedIcon className={classes.iconSideBar}/>,
        link: '/'
    },
]
  return (
    <>
      <Drawer
      className={classes.drawer}
        variant="permanent"
        classes={{ paper: classes.paper }}
      >
        <Toolbar />
        <Grid>
        <List >
          {SideBarData.map((text, index) => (
            <ListItem  button key={index} >
              <ListItemIcon className={classes.listItem}>{text.icon}</ListItemIcon>
            </ListItem>
          ))}
        </List>
        </Grid>
      </Drawer>
      
      </>
  );
}
