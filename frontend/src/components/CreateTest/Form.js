import React, { useState,useEffect } from 'react'
import useStyles from "./Styles";
import Mail from '../Temple/Mail'
import {useForm, FormProvider} from 'react-hook-form'
import StudentInfor from "./StudentInfor";
import { NativeSelect, Paper, Button, GridList, InputLabel, Grid, Select, MenuItem, Card, CardContent, Box, GridListTileBar} from '@material-ui/core'
import FormInput from '../CustomTextField'
import axios from 'axios'
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Notification from "./Notification";

const top100Films = [
    { title: 'Ai là người đặt tên cho dòng sông'},
    { title: 'OOP là gì'},
    { title: 'Hôm nay là thứ mấy'},
    { title: 'Ngày sinh của Bác Hồ'},
]

const Form = ({ handleClose }) => {
    const classes = useStyles();
    const methods = useForm();
    const [options, setOptions] = useState([{id:1,name:'mot'},{id:2,name:'hai'}]);
    const [option, setOption] = useState('');
    const [valueAuto, setValueAuto]= useState([])
    const [notify, setNotify] = useState({isOpen:false,message:'',type:''})

    const handleChange = (event) => {
    setOption(event.target.value);
    };

    const handleClose1 = () => {
        setOption('');;
    };

    const fetchOption = async () =>{
        const result = await axios.get('https://secret-everglades-88476.herokuapp.com/template')
        .then(res => res.data)
        .catch(err => console.log(err))

        setOptions(result)
    }

    useEffect(() => {
        fetchOption()
    },[option])

    const onSubmit =  data => {
    // console.log({...data,option,question:valueAuto})
    // return
    const [text] = options.filter( a => a._id === option)
    axios.post('https://secret-everglades-88476.herokuapp.com/sendemail',{...data,text:text.text,question:valueAuto})
        .then(res => {
            console.log('hahaha')
        })
        .catch(err => console.log(err))
    };

    const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
    const checkedIcon = <CheckBoxIcon fontSize="small" />;
    const handleChangeAutocomplete = (event, value) => {
        setValueAuto(value)
    }
    return (
        <Grid container spacing={2} >
            <Grid item xs={12} sm={5}>
            <Paper className={classes.paper}>
                <FormProvider { ...methods}>
                    <form onSubmit={methods.handleSubmit(onSubmit)} >
                        <Grid container spacing={3}>
                            <FormInput name='subject' label='Subject'/>
                            <Grid item xs={12} >
                            <Autocomplete
                            multiple
                            id="checkboxes-tags-demo"
                            options={top100Films}
                            disableCloseOnSelect
                            onChange={handleChangeAutocomplete}
                            getOptionLabel={(option) => option.title}
                            renderOption={(option, { selected }) => (
                                <React.Fragment>
                                <Checkbox
                                    icon={icon}
                                    checkedIcon={checkedIcon}
                                    style={{ marginRight: 8 }}
                                    checked={selected}
                                />
                                {option.title}
                                </React.Fragment>
                            )}
                            style={{ width: 500 }}
                            renderInput={(params) => (
                                <TextField {...params} variant="outlined" label="Questions" />
                            )}
                            />
                            </Grid>

                            <StudentInfor />
                            
                            <Grid item xs={12} sm={8}>
                                <InputLabel style={{minWidth: 400}}>Send with Invitation (choose the template)</InputLabel>
                                <NativeSelect
                                value={option}
                                onChange={handleChange}
                                style={{minWidth: 300}}
                                name='template'
                                >
                                <option aria-label="None" key={0} value="" />
                                {options.map((op,index) => <option key={op._id} value={op._id}>{op.name}</option>)}
                                <option key={1} value='+'>Add New</option>
                                </NativeSelect>
                            </Grid>

                        </Grid>
                    <br />
                    <div className={classes.buttons}>
                    <Button className={classes.button} variant="contained" onClick={handleClose}>Cancel</Button>
                    <Button className={classes.button} variant="contained" color="primary" type='submit'>Invite</Button>
                    </div>          
                    </form>
                </FormProvider>
            </Paper>
            </Grid>
            <Grid item xs={12} sm={7}>
                {
                    option === '+' && <Mail handleClose1={handleClose1} /> 
                }
            </Grid>
            <Notification notify={notify} setNotify={setNotify} />
        </Grid> 
    )
}

export default Form
