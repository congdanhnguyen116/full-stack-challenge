import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

const Notification = ({notify, setNotify}) => {
    const handleClose = (event, reason) => {
        setNotify({
            ...notify,isOpen: false
        })
    }
    return (
        <div>
            <Snackbar open={notify.isOpen} autoHideDuration={2000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={notify.type}>
                    {notify.message}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default Notification
