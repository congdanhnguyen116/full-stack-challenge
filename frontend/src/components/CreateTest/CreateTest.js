import React, { useState, useEffect } from 'react'
import useStyles from './Styles'
import { NativeSelect, Typography, Button, GridList, InputLabel, Grid, Select, MenuItem, Card, CardContent, Box, GridListTileBar} from '@material-ui/core'
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import FormInput from '../CustomTextField'
import {useForm, FormProvider} from 'react-hook-form'
import Mail from '../Temple/Mail'
import StudentInfor from "./StudentInfor";
import axios from 'axios'
import Form from './Form'
const Input = () => {
    return (
        <Card style={{ width: 300}}>
            <CardContent>
            <FormInput name='name' label='Name'/>
            <FormInput name='email' label='Email'/>
            <FormInput name='deadline' label='Deadline'/>
            </CardContent>
        </Card>
    )
  };

const CreateTest = () => {
    const classes = useStyles();

    const [open, setOpen] = React.useState(false);


    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    return (<>
        <main >
            <Toolbar/>
                <main className={classes.layout}>
                    <Grid container spacing={2} >
                    <Grid item container fullWidth>
                    <Button variant="contained" color="primary"  onClick={handleClickOpen} >Add New Test +</Button>
                    {open && <Form handleClose={handleClose}/>}
                    </Grid>
                </Grid>
                </main>
        </main>
    </>)
}

export default CreateTest
